This package contains the scripts and sources to drive Michael Ekstrand and
John Riedl's RecSys 2012 paper “When Recommenders Fail: Identifying and
Predicting Recommender Failure for Evidence-Based Algorithm Selection and
Combination”, updated for LensKit 2.1 as used in Michael's Ph.D dissertation.

Dependencies:

- Java 7

- R

- R 'packrat' package (the remainder of the dependencies are automatically
  installed with Packrat)

To run:

1. Download the ML-10M data set from http://grouplens.org/datasets/movielens
   and unpack it into a directory called `data/ml-10m` (the `ratings.dat` file
   should reside directly in `data/ml-10m`).

2. Run and analyze the experiment with `./gradlew run`.

This will produce a file `build/analysis.html` that contains the interesting
analysis outputs.  It will also save the analysis results to
`build/analysis.Rdata`, where they can be pulled into to knitr/Sweave scripts.

This version does not contain the LaTeX sources.  For that, check out the
`recsys-2012` branch.
