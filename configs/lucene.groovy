import org.apache.lucene.search.IndexSearcher
import org.grouplens.lenskit.ItemScorer
import org.grouplens.lenskit.baseline.BaselineScorer
import org.grouplens.lenskit.baseline.ItemMeanRatingItemScorer
import org.grouplens.lenskit.baseline.MeanDamping
import org.grouplens.lenskit.baseline.UserMeanBaseline
import org.grouplens.lenskit.baseline.UserMeanItemScorer
import org.grouplens.lenskit.knn.NeighborhoodSize
import org.grouplens.lenskit.knn.item.ItemItemScorer
import org.grouplens.lenskit.knn.item.ModelSize
import org.grouplens.lenskit.knn.item.model.ItemItemModel
import org.grouplens.lenskit.tags.data.IndexFile
import org.grouplens.lenskit.tags.data.IndexSearcherProvider
import org.grouplens.lenskit.tags.data.IndexedItemItemModel
import org.grouplens.lenskit.transform.normalize.BaselineSubtractingUserVectorNormalizer
import org.grouplens.lenskit.transform.normalize.UserVectorNormalizer
import org.grouplens.lenskit.vectors.similarity.SimilarityDamping

bind ItemScorer to ItemItemScorer
bind (BaselineScorer, ItemScorer) to UserMeanItemScorer
bind (UserMeanBaseline, ItemScorer) to ItemMeanRatingItemScorer
set MeanDamping to 25

bind UserVectorNormalizer to BaselineSubtractingUserVectorNormalizer
set SimilarityDamping to 100

set NeighborhoodSize to 40
set ModelSize to 500

bind ItemItemModel to IndexedItemItemModel
bind IndexSearcher toProvider IndexSearcherProvider
set IndexFile to new File("data/ml-10m.idx")
