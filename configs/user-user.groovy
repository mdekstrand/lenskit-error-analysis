import org.grouplens.lenskit.ItemScorer
import org.grouplens.lenskit.baseline.*
import org.grouplens.lenskit.knn.NeighborhoodSize
import org.grouplens.lenskit.knn.user.NeighborFinder
import org.grouplens.lenskit.knn.user.SnapshotNeighborFinder
import org.grouplens.lenskit.knn.user.UserSimilarity
import org.grouplens.lenskit.knn.user.UserUserItemScorer
import org.grouplens.lenskit.transform.normalize.*
import org.grouplens.lenskit.vectors.similarity.CosineVectorSimilarity
import org.grouplens.lenskit.vectors.similarity.VectorSimilarity

bind ItemScorer to UserUserItemScorer
bind (BaselineScorer, ItemScorer) to UserMeanItemScorer
bind (UserMeanBaseline, ItemScorer) to ItemMeanRatingItemScorer
set MeanDamping to 25
bind NeighborFinder to SnapshotNeighborFinder

at (UserUserItemScorer) {
    bind UserVectorNormalizer to DefaultUserVectorNormalizer
    within (UserVectorNormalizer) {
        bind VectorNormalizer to MeanVarianceNormalizer
    }
}

within (NeighborFinder) {
    bind UserVectorNormalizer to BaselineSubtractingUserVectorNormalizer
    within (UserVectorNormalizer) {
        bind (UserMeanBaseline, ItemScorer) to GlobalMeanRatingItemScorer
    }
}

within (UserSimilarity) {
    bind VectorSimilarity to CosineVectorSimilarity
}

set NeighborhoodSize to 30
