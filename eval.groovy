import org.grouplens.lenskit.eval.data.crossfold.RandomOrder
import org.grouplens.lenskit.eval.metrics.predict.CoveragePredictMetric
import org.grouplens.lenskit.eval.metrics.predict.MAEPredictMetric
import org.grouplens.lenskit.eval.metrics.predict.NDCGPredictMetric
import org.grouplens.lenskit.eval.metrics.predict.RMSEPredictMetric

def mlRandom = target("crossfold") {
    pack {
        dataset crossfold("random") {
            source {
                file "$config.dataDir/ml-10m/ratings.dat"
                    delimiter "::"
                    domain {
                        minimum 0.5
                            maximum 5
                            precision 0.5
                    }
            }
            order RandomOrder
                holdoutFraction 0.2
                partitions 5
                train "$config.dataDir/split/ml10m-random.%d.train.csv.gz"
                test "$config.dataDir/split/ml10m-random.%d.test.csv.gz"
        }
        includeTimestamps false
    }
}

def probeSplits = target("makeProbe") {
    requires mlRandom
    perform {
        def results = []
        for (ds in mlRandom.get()) {
            def n = ds.attributes['Partition']
            results << pack({
                dataset probeSplit("test.users.${n}") {
                    input ds
                    train "$config.dataDir/split/hybrid.${n}.train.csv.gz"
                    test "$config.dataDir/split/hybrid.${n}.probe.csv.gz"
                }
                includeTimestamps false
            })
        }
        results
    }
}

target("probe") {
    requires probeSplits
    trainTest("probe") {
        output "build/probe-aggregated.csv"
        // userOutput "build/per-user.csv.gz"
        predictOutput "build/probe-predictions.csv.gz"
        separateAlgorithms true

        metric MAEPredictMetric
        metric RMSEPredictMetric

        dataset probeSplits

        algorithm 'configs/mean.groovy', name: 'Mean'
        algorithm 'configs/user-user.groovy', name: 'UserUser'
        algorithm 'configs/item-item.groovy', name: 'ItemItem'
        algorithm 'configs/funksvd.groovy', name: 'FunkSVD'
        algorithm 'configs/lucene.groovy', name: 'Lucene'
    }
}

target("master") {
    requires mlRandom
    trainTest("master") {
        output "build/aggregated.csv"
            userOutput "build/per-user.csv.gz"
            predictOutput "build/individual.csv.gz"
            separateAlgorithms true

            metric CoveragePredictMetric
            metric MAEPredictMetric
            metric RMSEPredictMetric
            metric NDCGPredictMetric

            dataset mlRandom

            algorithm 'configs/mean.groovy', name: 'Mean'
            algorithm 'configs/user-user.groovy', name: 'UserUser'
            algorithm 'configs/item-item.groovy', name: 'ItemItem'
            algorithm 'configs/funksvd.groovy', name: 'FunkSVD'
            algorithm 'configs/lucene.groovy', name: 'Lucene'
    }
}
