package org.grouplens.lenskit.tags;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Ekstrand
 */
public class Movie {
    private final long id;
    private final String title;
    private List<String> tags;
    private List<String> genres;

    public Movie(long id, String title) {
        this.id = id;
        this.title = title;
        tags = new ArrayList<String>();
        genres = new ArrayList<String>();
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getTags() {
        return tags;
    }
    
    public void addTag(String tag) {
        tags.add(tag);
    }
    
    public void addGenre(String genre) {
        genres.add(genre);
    }

    public List<String> getGenres() {
        return genres;
    }
}
