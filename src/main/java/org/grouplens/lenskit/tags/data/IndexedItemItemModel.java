package org.grouplens.lenskit.tags.data;

import it.unimi.dsi.fastutil.longs.LongSortedSet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similar.MoreLikeThis;
import org.grouplens.lenskit.collections.LongUtils;
import org.grouplens.lenskit.core.Transient;
import org.grouplens.lenskit.data.dao.ItemDAO;
import org.grouplens.lenskit.knn.item.ModelSize;
import org.grouplens.lenskit.knn.item.model.ItemItemModel;
import org.grouplens.lenskit.scored.ScoredId;
import org.grouplens.lenskit.scored.ScoredIdListBuilder;
import org.grouplens.lenskit.scored.ScoredIds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Michael Ekstrand
 */
public class IndexedItemItemModel implements ItemItemModel {
    private static Logger logger = LoggerFactory.getLogger(IndexedItemItemModel.class);

    private final IndexSearcher searcher;
    private final LongSortedSet universe;
    private final int toFetch;

    @Inject
    public IndexedItemItemModel(@Transient ItemDAO dao,
                                IndexSearcher index,
                                @ModelSize int nnbrs) {
        searcher = index;
        universe = LongUtils.packedSet(dao.getItemIds());
        toFetch = nnbrs;
        logger.debug("initializing indexed model with size {}", nnbrs);
    }

    @Override
    public LongSortedSet getItemUniverse() {
        return universe;
    }

    @Nonnull
    @Override
    public List<ScoredId> getNeighbors(long item) {
        try {
            Term term = new Term("movie", Long.toString(item));
            Query tq = new TermQuery(term);
            TopDocs docs = searcher.search(tq, 1);
            if (docs.totalHits > 1) {
                logger.warn("found multiple matches for {}", item);
            } else if (docs.totalHits == 0) {
                logger.warn("could not find movie {}", item);
                return Collections.emptyList();
            }

            int docid = docs.scoreDocs[0].doc;
            Document doc = searcher.doc(docid);
            Long mid = Long.parseLong(doc.get("movie"));
            if (mid != item) {
                logger.error("retrieved document doesn't match ({} != {})", mid, item);
                return Collections.emptyList();
            }
            logger.trace("movie {} has index {}", item, docid);
            logger.trace("finding neighbors for movie {} ({})", item, doc.get("title"));

            MoreLikeThis mlt = new MoreLikeThis(searcher.getIndexReader());
            mlt.setFieldNames(new String[]{"title", "genres", "tags"});
            Query q = mlt.like(docid);
            TopDocs results = searcher.search(q, toFetch + 1);

            logger.trace("index returned {} of {} similar movies",
                         results.scoreDocs.length, results.totalHits);

            ScoredIdListBuilder rbld = ScoredIds.newListBuilder(toFetch);
            for (ScoreDoc sd: results.scoreDocs) {
                Document nbrdoc = searcher.doc(sd.doc);
                long id = Long.parseLong(nbrdoc.get("movie"));
                if (id != item) {
                    rbld.add(id, sd.score);
                }
            }
            logger.trace("returning {} neighbors", rbld.size());
            return rbld.build();
        } catch (IOException e) {
            throw new RuntimeException("IO error finding neighbors", e);
        }
    }
}
