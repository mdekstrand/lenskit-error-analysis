package scripts

import resource._
import collection.JavaConverters._
import java.io.File
import org.scalaquery.session.Database.threadLocalSession
import org.scalaquery.session._
import org.scalaquery.ql.extended.SQLiteDriver.Implicit._
import csv.CsvFile
import db._
import com.beust.jcommander.{Parameters, Parameter}
import org.grouplens.lenskit.data.dao.SimpleFileRatingDAO
import org.grouplens.lenskit.data.event.{Rating}


/**
 * @author Michael Ekstrand
 */
@Parameters(commandNames=Array("import-db"),
            commandDescription = "import ratings data to database")
object ImportData extends Script("import-db") {
  @Parameter(names=Array("-db"), description = "database file to use")
  var db = "ratingdata.db"

  @Parameter(names=Array("-split-dir"), description="directory containing splits")
  var splitDir: File = null

  @Parameter(names=Array("-ratings"), description="rating data")
  var ratings: File = null

  @Parameter(names=Array("-predictions"), description="predictions")
  var predictions: File = null

  @Parameter(names=Array("-no-train"), description="don't write train data")
  var noTrain: Boolean = false

  @Parameter(names=Array("-no-test"), description="don't write test data")
  var noTest: Boolean = false

  @Parameter(names=Array("-init"))
  var init = false

  def dbString = "jdbc:sqlite:" + db

  val Dataset = "ml10m-(\\w+)\\.(\\d+)\\.(\\w+).csv.gz".r

  def writePartition(file: File, dataset: DataSet.Value, partition: Int, table: PartitionTable) {
    info("loading ratings from %s".format(file))
    val factory = new SimpleFileRatingDAO.Factory(file, ",")
    threadLocalSession withTransaction {
      for (dao <- managed(factory.create());
           ratings <- managed(dao.getEvents(classOf[Rating]));
           r <- ratings.fast.asScala) {
        table.insert(dataset.id, partition, r.getUserId, r.getItemId)
      }
    }
  }

  def writeRatings(file: File) {
    info("loading ratings from %s".format(file))
    val factory = new SimpleFileRatingDAO.Factory(file, "::")
    threadLocalSession withTransaction {
      for (dao <- managed(factory.create());
           ratings <- managed(dao.getEvents(classOf[Rating]));
           r <- ratings.fast.asScala) {
        Ratings.insert(r.getUserId, r.getItemId, r.getPreference.getValue, r.getTimestamp)
      }
    }
  }
  
  def writePredictions(file: File) {
    info("loading predictions from %s".format(file))
    threadLocalSession withTransaction {
      for ((field, _) <- CsvFile(file, true, true)) {
        Predictions insert (
          DataSet(field('DataSet)).id,
          field('Partition).toInt,
          field('Algorithm),
          field('User).toInt,
          field('Item).toInt,
          field('Prediction).toDouble
        )
      }
    }
  }

  def main(args: Array[String]) {
    parseArgs(args)
    info("opening database %s".format(dbString))
    val db = Database.forURL(dbString)
    db withSession {
      if (init) {
        info("creating tables")
        (Ratings.ddl ++ Predictions.ddl ++ TestRatings.ddl ++ TrainRatings.ddl).create
      }
      Option(ratings).foreach(writeRatings)
      Option(predictions).foreach(writePredictions)
      for (dir <- Option(splitDir); fn <- dir.list()) {
        val file = new File(splitDir, fn)
        fn match {
          case Dataset(ds, p, "train") if !noTrain =>
            writePartition(file, DataSet(ds), p.toInt, TrainRatings)
          case Dataset(ds, p, "test") if !noTest =>
            writePartition(file, DataSet(ds), p.toInt, TestRatings)
          case _ => info("ignoring file %s".format(file))
        }
      }
    }
  }
}
