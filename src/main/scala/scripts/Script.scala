package scripts

import grizzled.slf4j.{Logger, Logging}
import com.beust.jcommander.{Parameters, Parameter, JCommander}


/**
 * @author Michael Ekstrand
 */

class Script(name: String) extends Logging {

  Class.forName("org.sqlite.JDBC")

  override val logger = Logger(name)

  @Parameter(names=Array("-help", "-?", "-h"))
  var help = false

  protected  def parseArgs(args: Array[String]) {
    val parse = new JCommander(this, args: _*)
    if (help) {
      parse.usage()
      sys.exit(2)
    }
    parse
  }
}
