package scripts

import csv.CsvFile
import collection.breakOut
import com.csvreader.CsvWriter
import java.nio.charset.Charset
import java.util.zip.GZIPOutputStream
import java.io.{File, FileOutputStream}
import com.beust.jcommander.Parameters

/**
 * @author Michael Ekstrand
 */

case class Row(algorithm: String,
               dataset: String, partition: Int,
               user: Long, item: Long,
               rating: Double, prediction: Double)

object Row {
  def apply(rec: (Map[Symbol,String], Seq[String])): Row = {
    val (f, _) = rec
    Row(
      f('Algorithm),
      f('DataSet),
      f('Partition).toInt,
      f('User).toLong,
      f('Item).toLong,
      f('Rating).toDouble,
      f('Prediction).toDouble
    )
  }
}

case class Key(dataset: String, partition: Int, user: Long, item: Long)

@Parameters(commandNames=Array("tilt-matrix"))
object TiltMatrix extends Script("tilt-matrix") {
  private def write(columns: Seq[String], rows: Map[Key,Map[String,Double]], outf: File) {
    val ostream = new GZIPOutputStream(new FileOutputStream(outf))
    val csv = new CsvWriter(ostream, ',', Charset.forName("UTF-8"))
    csv.write("DataSet")
    csv.write("Partition")
    csv.write("User")
    csv.write("Item")
    columns.foreach(csv.write _)
    csv.endRecord()
    for ((k,data) <- rows) {
      csv.write(k.dataset)
      csv.write(k.partition.toString)
      csv.write(k.user.toString)
      csv.write(k.item.toString)
      for (c <- columns) {
        data.get(c) match {
          case Some(v) => csv.write(v.toString)
          case None => csv.write("")
        }
      }
      csv.endRecord()
    }
    csv.close()
    info("%s: wrote %d records".format(outf, rows.size))
  }

  private def readData(file: File): (Seq[String], Map[Key,Map[String,Double]]) = {
    val algos = Set.newBuilder[String]
    val data = new collection.mutable.HashMap[Key,Map[String,Double]]
    val ratings = new collection.mutable.HashMap[Key,Double]
    for (row <- CsvFile(file, true).map(Row.apply _)) {
      algos += row.algorithm
      val key = Key(row.dataset, row.partition, row.user, row.item)
      ratings += key -> row.rating
      if (data.contains(key)) {
        data(key) += row.algorithm -> row.prediction
      } else {
        data += key -> Map(row.algorithm -> row.prediction)
      }
    }
    val columns = "Rating" +: algos.result().toSeq
    val rows: Map[Key,Map[String,Double]] =
      (for ((k,m) <- data) yield k -> (m + ("Rating" -> ratings(k))))(breakOut)
    (columns, rows)
  }

  def main(args: Array[String]) {
    parseArgs(args)
    info("reading individual.csv.gz")
    val (columns, records) = readData(new File("individual.csv.gz"))
    info("read %d records".format(records.size))
    write(columns, records, new File("prediction-matrix.csv.gz"))
    info("output written to prediction-matrix.csv.gz")
  }
}
