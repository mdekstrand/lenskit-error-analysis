CREATE INDEX rating_user_item_idx ON ratings (user, item);
CREATE INDEX rating_user_idx ON ratings (user);
CREATE INDEX rating_item_idx ON ratings (item);

CREATE INDEX prediction_idx ON predictions (dataset, algorithm, user, item);

CREATE INDEX test_user_item_idx ON test_ratings (user, item);
CREATE INDEX train_user_item_idx ON train_ratings (user, item);
CREATE INDEX test_ds_idx ON test_ratings (dataset,partition);
CREATE INDEX train_ds_idx ON train_ratings (dataset,partition);

CREATE TABLE algorithms AS SELECT DISTINCT algorithm FROM predictions;

ANALYZE;
